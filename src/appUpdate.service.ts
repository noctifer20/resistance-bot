import { Update, Ctx, Start, Help, On, Action, Sender } from 'nestjs-telegraf';
import { Context } from 'telegraf';
import { format } from 'date-fns';
import { FailSafeDecorator } from 'src/decorators/fail-safe.decorator';
import { Logger } from '@nestjs/common';

type SenderType = {
  username: string;
};

@Update()
export class AppUpdateService {
  hiButtonMarkup = [{ text: 'Hi', callback_data: 'hi' }];
  byeButtonMarkup = [{ text: 'Bye', callback_data: 'bye' }];

  constructor(private log: Logger) {}

  @Start()
  @FailSafeDecorator
  async start(@Ctx() ctx: Context, @Sender() sender: SenderType) {
    this.log.debug(`start for ${sender.username}`);

    await ctx.reply('Welcome', {
      reply_markup: {
        inline_keyboard: [this.hiButtonMarkup],
      },
    });
  }

  @Help()
  @FailSafeDecorator
  async help(@Ctx() ctx: Context, @Sender() sender: SenderType) {
    this.log.debug(`help for ${sender.username}`);

    await ctx.reply(`List of available commands`, {
      reply_markup: {
        inline_keyboard: [this.hiButtonMarkup, this.byeButtonMarkup],
      },
    });
  }

  @On('message')
  @FailSafeDecorator
  async on404(@Ctx() ctx: Context, @Sender() sender: SenderType) {
    this.log.debug(`404 for ${sender.username}`);

    await ctx.reply(`Sorry could not recognise command`, {
      reply_markup: {
        inline_keyboard: [this.hiButtonMarkup, this.byeButtonMarkup],
      },
    });
  }

  @Action('hi')
  @FailSafeDecorator
  async onHi(@Ctx() ctx: Context, @Sender() sender: SenderType) {
    this.log.debug(`hi for ${sender.username}`);

    await ctx.reply(`Hey ${sender.username}`, {
      reply_markup: {
        inline_keyboard: [this.byeButtonMarkup],
      },
    });
  }

  @Action('bye')
  @FailSafeDecorator
  async onBye(@Ctx() ctx: Context, @Sender() sender: SenderType) {
    this.log.debug(`bye for ${sender.username}`);

    await ctx.reply(`Bye at ${format(new Date(), 'hh:mm:ss dd/MM/yyyy')}`, {
      reply_markup: {
        inline_keyboard: [this.hiButtonMarkup],
      },
    });
  }
}
