import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, {
    logger: process.env.DEBUG === 'true',
  });
  await app.listen(() => {
    console.log('Microservice is running');
  });
}
bootstrap();
