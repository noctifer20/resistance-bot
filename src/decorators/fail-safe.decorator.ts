import { Context } from 'telegraf';
import { Logger } from '@nestjs/common';

/**
 * Catches internal errors and sends information to User.
 *
 * @name FailSafeDecorator
 * @param target
 * @param propertyName
 * @param descriptor
 * @constructor
 */
export const FailSafeDecorator = (
  target: any,
  propertyName: string,
  descriptor: TypedPropertyDescriptor<
    (crx: Context, ...args: any) => Promise<void>
  >,
) => {
  const method = descriptor.value;
  descriptor.value = async function (...args: [crx: Context, ...args: any]) {
    try {
      // call method with error handling
      return await method.apply(this, args);
    } catch (error) {
      // log error with injected logger
      (this.log as Logger)?.error(error);
      // send message with no-op on error
      try {
        await args[0].reply('Sorry, seems the government got us!');
      } catch {}
    }
  };
};
