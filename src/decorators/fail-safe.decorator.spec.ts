import { FailSafeDecorator } from './fail-safe.decorator';
import type { Context } from 'telegraf';

describe('FailSafeDecorator', () => {
  let mockCtx: {
    new (...args: any): Context;
  };
  let mockClass: {
    log: {
      error: jest.FunctionLike;
    };
    failingMethod(ctx: Context): void;
    passingMethod(ctx: Context): void;
  };

  beforeEach(() => {
    mockCtx = jest.createMockFromModule<{
      Context: {
        new (...args: any): Context;
      };
    }>('telegraf/lib/context').Context;
    mockClass = {
      log: {
        error: jest.fn(),
      },
      failingMethod(ctx: Context) {
        throw new Error('fail');
      },
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      passingMethod(ctx: Context) {},
    };
  });

  it('should send fail message to user on error', async () => {
    const descriptor = Object.getOwnPropertyDescriptor(
      mockClass,
      'failingMethod',
    );
    const ctx = new mockCtx();
    FailSafeDecorator(mockClass, 'failingMethod', descriptor);

    await descriptor.value.apply(mockClass, [ctx]);

    expect(mockClass.log.error).toBeCalledTimes(1);
    expect(ctx.reply).toBeCalledTimes(1);
  });

  it('should pass on no error', async () => {
    const descriptor = Object.getOwnPropertyDescriptor(
      mockClass,
      'passingMethod',
    );
    const ctx = new mockCtx();
    FailSafeDecorator(mockClass, 'passingMethod', descriptor);

    await descriptor.value.apply(mockClass, [ctx]);

    expect(mockClass.log.error).toBeCalledTimes(0);
    expect(ctx.reply).toBeCalledTimes(0);
  });

  it('should not fail if no logger is present', async () => {
    delete mockClass.log;

    const descriptor = Object.getOwnPropertyDescriptor(
      mockClass,
      'failingMethod',
    );
    const ctx = new mockCtx();
    FailSafeDecorator(mockClass, 'failingMethod', descriptor);

    await descriptor.value.apply(mockClass, [ctx]);

    expect(ctx.reply).toBeCalledTimes(1);
  });
});
