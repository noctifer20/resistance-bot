import type { Context } from 'telegraf';
import type { Logger } from '@nestjs/common';
import { AppUpdateService } from './appUpdate.service';

jest.mock('./decorators/fail-safe.decorator');
jest.mock('date-fns', () => ({
  format: () => 'MOCKED_TIME',
}));

const byeButtonMarkup = [{ text: 'Bye', callback_data: 'bye' }];
const hiButtonMarkup = [{ text: 'Hi', callback_data: 'hi' }];

describe('AppUpdateSevice', () => {
  let mockSender: { username: string };
  let mockCtx: {
    new (...args: any): Context;
  };
  let mockLog: {
    debug: jest.FunctionLike;
  };

  beforeEach(() => {
    mockCtx = jest.createMockFromModule<{
      Context: {
        new (...args: any): Context;
      };
    }>('telegraf/lib/context').Context;

    mockSender = {
      username: Math.random().toString().substr(2),
    };
    mockLog = {
      debug: jest.fn(),
    };
  });

  it('should handle start event', async () => {
    const service = new AppUpdateService(mockLog as Logger);
    const ctx = new mockCtx();

    await service.start(ctx, mockSender);

    expect(mockLog.debug).toBeCalledTimes(1);
    expect(ctx.reply).toBeCalledTimes(1);
  });

  it('should handle help event', async () => {
    const service = new AppUpdateService(mockLog as Logger);
    const ctx = new mockCtx();

    await service.help(ctx, mockSender);

    expect(mockLog.debug).toBeCalledTimes(1);
    expect(ctx.reply).toBeCalledWith('List of available commands', {
      reply_markup: {
        inline_keyboard: [hiButtonMarkup, byeButtonMarkup],
      },
    });
  });

  it('should handle 404 event', async () => {
    const service = new AppUpdateService(mockLog as Logger);
    const ctx = new mockCtx();

    await service.on404(ctx, mockSender);

    expect(mockLog.debug).toBeCalledTimes(1);
    expect(ctx.reply).toBeCalledWith('Sorry could not recognise command', {
      reply_markup: {
        inline_keyboard: [hiButtonMarkup, byeButtonMarkup],
      },
    });
  });

  it('should handle hi event', async () => {
    const service = new AppUpdateService(mockLog as Logger);
    const ctx = new mockCtx();

    await service.onHi(ctx, mockSender);

    expect(mockLog.debug).toBeCalledTimes(1);
    expect(ctx.reply).toBeCalledWith(`Hey ${mockSender.username}`, {
      reply_markup: {
        inline_keyboard: [byeButtonMarkup],
      },
    });
  });

  it('should handle bye event', async () => {
    const service = new AppUpdateService(mockLog as Logger);
    const ctx = new mockCtx();

    await service.onBye(ctx, mockSender);

    expect(mockLog.debug).toBeCalledTimes(1);
    expect(ctx.reply).toBeCalledWith('Bye at MOCKED_TIME', {
      reply_markup: {
        inline_keyboard: [hiButtonMarkup],
      },
    });
  });
});
