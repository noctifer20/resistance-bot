## Description

This package contains microservice for [resistance-bot](https://t.me/bots_resistance_bot) operations.

## Supported Events And Handlers
[AppUpdateServices](./src/appUpdate.service.ts) is used to get updates for resistance-bot using long-polling.

| Event | Description                                                           | Handler |
|-------|-----------------------------------------------------------------------|---------|
| start | Called when User presses the `Start` button or types `/start` in chat | onStart |
| help  | Called when User types `/help` in chat                                | onHelp  |
| hi    | Called when User presses the `Hi` button                              | onHi    |
| bye   | Called when User presses the `Bye` button                             | onBye   |
| 404   | Called when User types not supported command                          | on404   |

## Error Handling

[FailSafeDecorator](./src/decorators/fail-safe.decorator.ts) is used to handle errors on AppUpdateService methods, 
by wrapping a method with error boundary. It logs Errors using [NestJs Logger](https://docs.nestjs.com/techniques/logger) instance and send a failure message to User.

## Running the Microservice

```bash
$ yarn start
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Environment Variables
| Name           | Description                                  | Default Value | Required |
|----------------|----------------------------------------------|---------------|----------|
| TELEGRAM_TOKEN | Token to connect to Telegram Bot             | N/A           | YES      |
| DEBUG          | Enables logging                              | false         | NO       |

## Test

```bash
# unit tests
$ yarn test

# test coverage
$ yarn test:cov
```

## Coverage
Coverage on non-arbitrary files is 100%

| File                    | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s |
|-------------------------|---------|----------|---------|---------|-------------------|
| appUpdate.service.ts    |     100 |      100 |     100 |     100 |                   |
| fail-safe.decorator.ts  |     100 |      100 |     100 |     100 |                   |
